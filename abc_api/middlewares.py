from django.db.models import signals
from django.utils.functional import curry
import datetime
import os.path

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response,get_object_or_404
from django.core.urlresolvers import reverse
 

class DisableCSRF(object):
    def process_request(self, request):
            setattr(request, '_dont_enforce_csrf_checks', True)