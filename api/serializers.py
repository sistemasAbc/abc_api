from models import Cliente, Empleado, OrdenServicio, OrdenServicioDetalle, OrdenVenta
from rest_framework import serializers
import datetime
from django.db import connection, connections
from django.db.models import Max, Sum

import ConfigParser
from tempfile import mkstemp
from shutil import move
from os import remove, close, chmod

class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = (
            'id',
            'nombre',
            'razon_social',
            'rfc',
            'clave',
            'id_cltst',
            'updated_on',
            'credito_asignado',
            'saldo',
            'credito_disponible',
            'tiene_credito_congelado',
        )


class EmpleadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empleado
        fields = (
            'id',
            'nombre',
            'paterno',
            'materno',
            'updated_on',
        )


class OrdenServicioDetalleSerializer(serializers.ModelSerializer):

    orden_servicio_id = serializers.IntegerField(required=False)
    id_srvoperacion = serializers.IntegerField(required=True)
    id_empempleado = serializers.IntegerField(required=True)
    nota = serializers.CharField(required=False)
    empresa = serializers.CharField(required=False, write_only=True)

    def create(self, validated_data):
        now = datetime.datetime.now()
        detalles = validated_data.pop('detalles', [])
        empresa = validated_data.pop('empresa', False)

        validated_data['cantidad'] = 1
        # validated_data['id_srvoperacion'] = 305
        validated_data['id_srvoperacionst'] = 4
        validated_data['id_pvrproveedor'] = 0
        validated_data['horasprecio'] = 0.00
        validated_data['horascosto'] = 0.00
        validated_data['costo'] = 0.00
        validated_data['ivafactura'] = 0.00

        # validated_data['fecha_factura'] = now.strftime('%Y-%m-%d')
        # validated_data['fecha_compromiso'] = now.strftime('%Y-%m-%d')

        validated_data['hora_ini'] = "00:00:00"
        validated_data['hora_fin'] = "00:00:00"
        validated_data['hrpausa'] = "00:00:00"
        validated_data['hrinicio'] = "00:00:00"
        
        validated_data['hrsacumuladas'] = 0.00
        validated_data['id_pvrfactura'] = 0
        validated_data['puntos'] = 0
        validated_data['monedero'] = 0
        validated_data['id_srvpaquete'] = 0
        validated_data['id_srvpromocion'] = 0

        ## Campos no obligatorios
        validated_data['tipodetalle'] = ""
        validated_data['taskcode'] = ""
        validated_data['taskname'] = ""
        validated_data['foliofactura'] = ""
        validated_data['puntos'] = 0
        validated_data['monedero'] = 0
        validated_data['id_srvpaquete'] = 0
        validated_data['id_srvpromocion'] = 0
        validated_data['razon_detiene'] = ""

        if empresa=="abz":
            orden_servicio_detalle = OrdenServicioDetalle.objects.create(**validated_data)
        if empresa=="tasa":
            orden_servicio_detalle = OrdenServicioDetalle.objects.using('tasa').create(**validated_data)

        return orden_servicio_detalle


    class Meta:
        model = OrdenServicioDetalle
        fields = (
            'id',
            'orden_servicio_id',
            'id_srvoperacion',
            'preciounitario',
            'precio',
            'id_empempleado',
            'id_ciatipoiva',
            'tasaiva',
            'fecha_ini',
            'fecha_fin',
            'nota',
            'empresa',
        )

class OrdenServicioSerializer(serializers.ModelSerializer):

    # grupo = serializers.CharField(required=True)
    observacion = serializers.CharField(required=False)
    ordencompra = serializers.CharField(required=False)
    id_srvostipo = serializers.IntegerField(required=True)
    id_srvossubtipo = serializers.IntegerField(required=False)
    id_ciasucursal = serializers.IntegerField(required=True)
    idsucinterno = serializers.IntegerField(required=False)
    idareainterno = serializers.IntegerField(required=False)
    folio = serializers.CharField(required=False)
    empresa = serializers.CharField(required=True, write_only=True)
        
    detalles = OrdenServicioDetalleSerializer(read_only=False, many=True)

    def validate(self, data):
        id = data.get('id', False)
        id_ciasucursal = data.get('id_ciasucursal', False)
        id_srvostipo = data.get('id_srvostipo', False)
        id_srvossubtipo = data.get('id_srvossubtipo', False)
        idsucinterno = data.get('idsucinterno', False)
        idareainterno = data.get('idareainterno', False)
        empresa = data.get('empresa', False)

        if not id_ciasucursal:
            raise serializers.ValidationError("Se requiere id_ciasucursal")
            
        if id > 0:
            raise serializers.ValidationError("No puedes actualizar")
        if id_srvostipo==5:
            if not id_srvossubtipo:
                raise serializers.ValidationError("Especifica subtipo de orden")
            if id_srvossubtipo==1:
                if not idsucinterno:
                    raise serializers.ValidationError("Especifica sucursal")
                if not idareainterno:
                    raise serializers.ValidationError("Especifica area")

        return data

    def create(self, validated_data):

        now = datetime.datetime.now()
        detalles = validated_data.pop('detalles', [])
        empresa = validated_data.pop('empresa', False)

        orden_venta = []
        if validated_data['contacto']:
            validated_data['contacto'] = validated_data['contacto'][0:29]
        else:
            validated_data['contacto'] = ""

        validated_data['id_srvequipo'] = 9144
        validated_data['id_cltpreferencialtarjeta'] = 0
        validated_data['idventanilla'] = 0
        validated_data['id_comov'] = 0
        validated_data['id_grsos'] = 0
        validated_data['id_srvcontrato'] = 0
        validated_data['id_pjtproyecto'] = 0
        validated_data['kilometraje'] = 10000
        validated_data['id_srvtipoinmovilizacion'] = 0
        # validated_data['fecha_inmovilizacion'] = fecha_default
        validated_data['hora_inmovilizacion'] = "00:00:00"
        validated_data['id_srvosst'] = 2 ##2 APROBADA..... CERRADA 4
        # validated_data['id_ciasucursal'] = 7
        validated_data['tasadescmo'] = 0.00
        validated_data['tasadescrfc'] = 0.00
        validated_data['id_ciamoneda'] = 1
        validated_data['tipocambio'] = 1.0000
        validated_data['com_subtotal'] = 0
        validated_data['com_iva'] = 0
        validated_data['com_retiva'] = 0
        validated_data['com_retisr'] = 0
        validated_data['com_importe'] = 0
        validated_data['puntos'] = 0
        validated_data['monedero'] = 0
        validated_data['fecha_apertura'] = now.strftime('%Y-%m-%d')
        validated_data['hora_apertura'] = now.strftime('%H:%M:%S') #"00:00:00" 
        # validated_data['fecha_cierre'] = now.strftime('%Y-%m-%d')
        validated_data['hora_cierre'] = "00:00:00"
        validated_data['contactoemail'] = ""
        validated_data['contactotel'] = "4150550"
        validated_data['contactocel'] = ""
        validated_data['idempcierre'] = 0
        validated_data['fecha_promentrega'] = now.strftime('%Y-%m-%d')
        validated_data['hora_promentrega'] = now.strftime('%H:%M:%S') #"00:00:00"
        validated_data['idosinterno'] = 0
        validated_data['idgtointerno'] = 0

        # validated_data['fecha_factura'] = now.strftime('%Y-%m-%d')
        validated_data['hora_factura'] = "00:00:00"
        validated_data['id_cltfactura'] = 0
        validated_data['idempfactura'] = 0
        # validated_data['fecha_cancelacion'] = now.strftime('%Y-%m-%d')
        validated_data['hora_cancelacion'] = "00:00:00"
        validated_data['idempcancela'] = 0
        # validated_data['servparticular'] = 0

        ## Campos no obligatorios
        validated_data['torreta'] = ''
        validated_data['inspeccion'] = ""
        validated_data['siniestro'] = ""
        validated_data['des_inmovilizacion'] = 0
        validated_data['facturaseparada'] = "S"
        validated_data['cierraovcomercial'] = "N"
        validated_data['desgloseiva'] = "S"
        validated_data['origenentrada'] = "Gruas"
        validated_data['express'] = "N"
        validated_data['refacturacion'] = "N"
        
        # validated_data['ventacredito'] = "S"

        if empresa=="abz":
            orden_servicio = OrdenServicio.objects.create(**validated_data)

            ## Detalles
            for detalle in detalles:
                detalle['orden_servicio_id'] = orden_servicio.pk
                detalle['empresa'] = empresa
                detalle['fecha_ini'] = now.strftime('%Y-%m-%d')
                detalle['fecha_fin'] = now.strftime('%Y-%m-%d')
                detalle['id_ciatipoiva'] = validated_data['id_ciatipoiva']
                detalle['tasaiva'] = validated_data['tasaiva']

                # detalle['id_empempleado'] = 245 ## todo

                d_serializer = OrdenServicioDetalleSerializer(data=detalle)
                if d_serializer.is_valid(raise_exception=True):
                    d_serializer.save()
            

            now = datetime.datetime.now()
            
            data_ov = {}
            data_ov['folio'] = 0
            data_ov['id_srvos'] = orden_servicio.pk
            data_ov['id_empempleado'] = orden_servicio.idasesor
            data_ov['id_cltcliente'] = orden_servicio.id_cltcliente
            data_ov['id_srvostipo'] = orden_servicio.id_srvostipo
            data_ov['id_ciasucursal'] = orden_servicio.id_ciasucursal
            data_ov['id_cltpreferencialtarjeta'] = 0
            data_ov['id_pjtproyecto'] = 0
            data_ov['fecha_ov'] = now.strftime('%Y-%m-%d')
            data_ov['hora_ov'] = now
            data_ov['id_comovtipo'] = 4 #Servicio
            data_ov['id_comovst'] = 1 ##Cotizacion
            data_ov['desgloseiva'] = "S"
            data_ov['ventacredito'] = "S"
            data_ov['tasadescuento'] = 0.00
            data_ov['iva'] = 0.00
            data_ov['retiva'] = 0.00
            data_ov['retisr'] = 0.00
            data_ov['importe'] = 0.00
            data_ov['id_cltfactura'] = 0
            data_ov['id_ciamoneda'] = 1
            data_ov['tipocambio'] = 1.0000
            data_ov['descuento'] = 0.00
            data_ov['ordencompra'] = ""
            data_ov['referencia'] = ""
            data_ov['nota'] = ""
            data_ov['entregapersona'] = ""
            data_ov['entregatelefono'] = ""
            data_ov['entregadireccion'] = ""
            data_ov['anterior'] = ""
            data_ov['puntos'] = 0.00
            data_ov['monedero'] = 0.00
            data_ov['id_ciaaduana'] = 0
            data_ov['hora_cierre'] = "00:00:00"
            data_ov['idempcierre'] = 0
            data_ov['hora_factura'] = "00:00:00"
            data_ov['idempfactura'] = 0
            data_ov['hora_cancelacion'] = "00:00:00"
            data_ov['idempcancela'] = 0
            data_ov['id_cltingresoforma'] = 0
            data_ov['cuentapago'] = ""
            data_ov['codigoale'] = ""
            
            orden_venta = OrdenVenta.objects.create(**data_ov)

            cursor = connection.cursor()
            query = """
                SELECT MAX(abs(folio)) AS max_folio
                FROM srvOS
                WHERE folio > 0
            """

            cursor.execute(query,[])
            max_folio = 0 
            for row in cursor.fetchall():
                if row[0]!=None:
                    max_folio = row[0]
            
            siguiente_folio_os = int(max_folio) + 1

            orden_servicio.id_comov = orden_venta.pk
            orden_servicio.folio = siguiente_folio_os
            orden_servicio.save()

            query = """
                SELECT MAX(abs(folio)) AS max_folio
                FROM comOV
                WHERE folio > 0 AND id_ciasucursal=%s
            """ %(orden_servicio.id_ciasucursal)

            cursor.execute(query,[])

            max_folio_ov = 0 
            for row in cursor.fetchall():
                if row[0]!= None:
                    max_folio_ov = row[0]
            
            siguiente_folio = int(max_folio_ov) + 1
            orden_venta.folio = siguiente_folio
            orden_venta.save()

            with open("/opt/lampp/htdocs/company/hdz/info.ini", "r+") as f:
                old = f.read() # read everything in the file
                for line in old.splitlines():
                    if 'SRV_FOLIO_AUTOMATICO_FOLIO' in line:
                        replace('/opt/lampp/htdocs/company/hdz/info.ini', line, '        SRV_FOLIO_AUTOMATICO_FOLIO = %s' % (siguiente_folio_os,))

        if empresa=="tasa":
            orden_servicio = OrdenServicio.objects.using('tasa').create(**validated_data)

            ## Detalles
            for detalle in detalles:
                detalle['orden_servicio_id'] = orden_servicio.pk
                detalle['empresa'] = empresa
                detalle['fecha_ini'] = now.strftime('%Y-%m-%d')
                detalle['fecha_fin'] = now.strftime('%Y-%m-%d')
                detalle['id_ciatipoiva'] = validated_data['id_ciatipoiva']
                detalle['tasaiva'] = validated_data['tasaiva']

                # detalle['id_empempleado'] = 245 ## todo

                d_serializer = OrdenServicioDetalleSerializer(data=detalle)
                if d_serializer.is_valid(raise_exception=True):
                    d_serializer.save()


            now = datetime.datetime.now()
            
            data_ov = {}
            data_ov['folio'] = 0
            data_ov['id_srvos'] = orden_servicio.pk
            data_ov['id_empempleado'] = orden_servicio.idasesor
            data_ov['id_cltcliente'] = orden_servicio.id_cltcliente
            data_ov['id_srvostipo'] = orden_servicio.id_srvostipo
            data_ov['id_ciasucursal'] = orden_servicio.id_ciasucursal
            data_ov['id_cltpreferencialtarjeta'] = 0
            data_ov['id_pjtproyecto'] = 0
            data_ov['fecha_ov'] = now.strftime('%Y-%m-%d')
            data_ov['hora_ov'] = now
            data_ov['id_comovtipo'] = 4 #Servicio
            data_ov['id_comovst'] = 1 ##Cotizacion
            data_ov['desgloseiva'] = "S"
            data_ov['ventacredito'] = "S"
            data_ov['tasadescuento'] = 0.00
            data_ov['iva'] = 0.00
            data_ov['retiva'] = 0.00
            data_ov['retisr'] = 0.00
            data_ov['importe'] = 0.00
            data_ov['id_cltfactura'] = 0
            data_ov['id_ciamoneda'] = 1
            data_ov['tipocambio'] = 1.0000
            data_ov['descuento'] = 0.00
            data_ov['ordencompra'] = ""
            data_ov['referencia'] = ""
            data_ov['nota'] = ""
            data_ov['entregapersona'] = ""
            data_ov['entregatelefono'] = ""
            data_ov['entregadireccion'] = ""
            data_ov['anterior'] = ""
            data_ov['puntos'] = 0.00
            data_ov['monedero'] = 0.00
            data_ov['id_ciaaduana'] = 0
            data_ov['hora_cierre'] = "00:00:00"
            data_ov['idempcierre'] = 0
            data_ov['hora_factura'] = "00:00:00"
            data_ov['idempfactura'] = 0
            data_ov['hora_cancelacion'] = "00:00:00"
            data_ov['idempcancela'] = 0
            data_ov['id_cltingresoforma'] = 0
            data_ov['cuentapago'] = ""
            data_ov['codigoale'] = ""
            
            orden_venta = OrdenVenta.objects.using('tasa').create(**data_ov)


            cursor = connections['tasa'].cursor()

            query = """
                SELECT MAX(abs(folio)) AS max_folio
                FROM srvOS
                WHERE folio > 0
            """
            cursor.execute(query,[])
            max_folio = 0 
            for row in cursor.fetchall():
                if row[0]!= None:
                    max_folio = row[0]
            
            siguiente_folio_os = int(max_folio) + 1

            orden_servicio.id_comov = orden_venta.pk
            orden_servicio.folio = siguiente_folio_os
            orden_servicio.save()

            query = """
                SELECT MAX(abs(folio)) AS max_folio
                FROM comOV
                WHERE folio > 0 AND id_ciasucursal=%s
            """ %(orden_servicio.id_ciasucursal)

            cursor.execute(query,[])

            max_folio_ov = 0 
            for row in cursor.fetchall():
                if row[0]!= None:
                    max_folio_ov = row[0]
            
            siguiente_folio = int(max_folio_ov) + 1
            orden_venta.folio = siguiente_folio
            orden_venta.save()


            with open("/opt/lampp/htdocs/company/tasa/info.ini", "r+") as f:
                old = f.read() # read everything in the file
                for line in old.splitlines():
                    if 'SRV_FOLIO_AUTOMATICO_FOLIO' in line:
                        replace('/opt/lampp/htdocs/company/tasa/info.ini', line, '        SRV_FOLIO_AUTOMATICO_FOLIO = %s' % (siguiente_folio_os,))

        return orden_servicio

    class Meta:
        model = OrdenServicio
        fields = (
            'id',
            'folio',
            'id_cltcliente',
            'ordencompra',
            'idasesor', ## id del empleado que lo crea
            'id_ciatipoiva', # IVA 1>16%, IVA 3>0%
            'tasaiva', # IVA 1>16%, IVA 3>0%
            
            'srv_subtotal',
            'srv_iva',
            'srv_retiva',
            'srv_retisr',
            'srv_importe',
            'observacion',
            'contacto', # Nombre del empleado

            'id_ciasucursal',
            'id_srvostipo',
            'id_srvossubtipo',
            'idsucinterno',
            'idareainterno',
            'ventacredito',
            'servparticular', #1 si es particular

            'detalles',
            'empresa',
        )
        read_only_fields = "folio,"





def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

    chmod(file_path, 0o777)