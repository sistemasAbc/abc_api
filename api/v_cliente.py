from models import Cliente, Sync
from serializers import ClienteSerializer
from django.contrib.contenttypes.models import ContentType
from datetime import datetime

## rest framework
from rest_framework import viewsets, filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework import status

class ClienteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        empresa = request.GET.get('empresa', None)
        sucursal = request.GET.get('sucursal', None)
        instance = None
        if empresa=="abz":
            instance = Cliente.objects.get(pk=pk)
            instance.SUCURSAL = sucursal
        if empresa=="tasa":
            instance = Cliente.objects.using('tasa').get(pk=pk)
            instance.EMPRESA = "tasa"
            instance.SUCURSAL = sucursal
        # instance = self.get_object()
        if not instance:
            return Response({'mensaje':'Cliente no identificado'}, status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def get_queryset(self):
        empresa = self.request.GET.get('empresa',None)
        queryset = Cliente.objects.none()
        if empresa=="abz":
            queryset = Cliente.objects.all()
        if empresa=="tasa":
            queryset = Cliente.objects.using('tasa').all()

        return queryset


    @list_route(methods=['get'])
    def sync(self, request):

        model = ContentType.objects.get_for_model(Cliente)
        empresa = request.GET.get('empresa',None)
        queryset = Cliente.objects.none()

        if empresa=="abz":
            queryset = Cliente.objects.all()

            try:
                sync = Sync.objects.filter(model=model).latest('last_synced')
            except Exception, e:
                sync = False

            if sync:
                queryset = queryset.filter(updated_on__gte=sync.last_synced)

            if queryset.count() > 0:        
                sync = Sync.objects.create(model=model, last_synced=datetime.now())
                sync.save()
            
        if empresa=="tasa":
            queryset = Cliente.objects.using('tasa').all()

            try:
                sync = Sync.objects.using('tasa').filter(model=model).latest('last_synced')
            except Exception, e:
                sync = False

            if sync:
                queryset = queryset.filter(updated_on__gte=sync.last_synced)
            
            if queryset.count() > 0:        
                sync = Sync.objects.using('tasa').create(model=model, last_synced=datetime.now())
                sync.save()
            

        serializer = ClienteSerializer(queryset, many=True)

        return Response(serializer.data)


    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
    http_method_names = ['get',]