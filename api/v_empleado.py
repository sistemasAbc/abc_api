from models import Empleado, Sync
from serializers import EmpleadoSerializer
from django.contrib.contenttypes.models import ContentType
from datetime import datetime

## rest framework
from rest_framework import viewsets, filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework import status

class EmpleadoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    def get_queryset(self):
        # queryset = Empleado.objects.filter(id_ciasucursal=7, id_empempleadost=1)
        queryset = Empleado.objects.all()

        if self.request.GET:
            folio = self.request.GET.get('folio',"")
            estatus = self.request.GET.getlist('estatus',None)

            if folio:
                queryset = queryset.filter(folio__icontains=folio)
            if estatus:
                queryset = queryset.filter(estatus__in=estatus)
            
        return queryset


    @list_route(methods=['get'])
    def sync(self, request):

        empresa = request.GET.get('empresa',None)
        sucursales = request.GET.getlist('sucursales',[])
        model = ContentType.objects.get_for_model(Empleado)
        queryset = Empleado.objects.none()

        # sucursales = [7,16]
        # sucursales = [3,4]
        
        if empresa=="abz":
            queryset = Empleado.objects.filter(id_ciasucursal__in=sucursales, id_empempleadost=1)

            try:
                sync = Sync.objects.filter(model=model).latest('last_synced')
            except Exception, e:
                sync = False

            if sync:
                queryset = queryset.filter(updated_on__gte=sync.last_synced)

            if queryset.count() > 0:        
                sync = Sync.objects.create(model=model, last_synced=datetime.now())
                sync.save()

        if empresa=="tasa":
            queryset = Empleado.objects.using('tasa').filter(id_ciasucursal__in=sucursales, id_empempleadost=1)

            try:
                sync = Sync.objects.using('tasa').filter(model=model).latest('last_synced')
            except Exception, e:
                sync = False

            if sync:
                queryset = queryset.filter(updated_on__gte=sync.last_synced)

            if queryset.count() > 0:
                model = ContentType.objects.get_for_model(Empleado)
                sync = Sync.objects.using('tasa').create(model=model, last_synced=datetime.now())
                sync.save()

        serializer = EmpleadoSerializer(queryset, many=True)

        return Response(serializer.data)


    queryset = Empleado.objects.none()
    serializer_class = EmpleadoSerializer
    http_method_names = ['get',]