from __future__ import unicode_literals
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.db.models import Max, Sum
import datetime
from datetime import date, time, timedelta
from django.db import connection, connections
from decimal import Decimal

class Cliente(models.Model):
    id = models.AutoField(db_column='id_cltCliente', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(max_length=80)
    razon_social = models.CharField(db_column='nombreFiscal', max_length=90)  # Field name made lowercase.
    rfc = models.CharField(max_length=13)
    
    appaterno = models.CharField(db_column='apPaterno', max_length=20)  # Field name made lowercase.
    apmaterno = models.CharField(db_column='apMaterno', max_length=20)  # Field name made lowercase.
    id_cltst = models.IntegerField(db_column='id_cltST')  # Field name made lowercase.
    idcobrador = models.IntegerField(db_column='idCobrador')  # Field name made lowercase.
    clave = models.CharField(max_length=6)
    curp = models.CharField(max_length=18)
    id_locestado = models.IntegerField(db_column='id_locEstado')  # Field name made lowercase.
    municipio = models.CharField(max_length=30)
    ciudad = models.CharField(max_length=30)
    colonia = models.CharField(max_length=40)
    calle = models.CharField(max_length=45)
    numero = models.CharField(max_length=6)
    numerointerior = models.CharField(db_column='numeroInterior', max_length=10)  # Field name made lowercase.
    cp = models.CharField(max_length=6)
    tel_casa = models.CharField(max_length=12)
    lada_casa = models.CharField(max_length=4)
    tel_trabajo = models.CharField(max_length=12)
    lada_trabajo = models.CharField(max_length=4)
    tel_fax = models.CharField(max_length=12)
    lada_fax = models.CharField(max_length=4)
    tel_celular = models.CharField(max_length=14)
    contacto = models.CharField(max_length=40)
    email = models.CharField(max_length=40)
    soloremision = models.CharField(db_column='soloRemision', max_length=1)  # Field name made lowercase.
    facturapublico = models.CharField(db_column='facturaPublico', max_length=1)  # Field name made lowercase.
    causanteisr = models.CharField(db_column='causanteISR', max_length=1)  # Field name made lowercase.
    causanteiva = models.CharField(db_column='causanteIVA', max_length=1)  # Field name made lowercase.
    id_clttipo = models.IntegerField(db_column='id_cltTipo')  # Field name made lowercase.
    id_compreciolista = models.IntegerField(db_column='id_comPrecioLista')  # Field name made lowercase.
    desccom = models.DecimalField(db_column='descCOM', max_digits=4, decimal_places=2)  # Field name made lowercase.
    descsrv = models.DecimalField(db_column='descSRV', max_digits=4, decimal_places=2)  # Field name made lowercase.
    preferencial = models.CharField(max_length=1)
    tarjetarequerida = models.CharField(db_column='tarjetaRequerida', max_length=1)  # Field name made lowercase.
    puntos = models.DecimalField(max_digits=10, decimal_places=2)
    monedero = models.DecimalField(max_digits=10, decimal_places=2)
    fecha_aperturacredito = models.DateField(db_column='fecha_aperturaCredito')  # Field name made lowercase.
    notas = models.TextField()
    rfc_personal = models.CharField(max_length=10)
    entrecalles = models.CharField(max_length=60)
    idvendedor = models.IntegerField(db_column='idVendedor')  # Field name made lowercase.
    reqordencompracom = models.CharField(db_column='reqOrdenCompraCOM', max_length=1)  # Field name made lowercase.
    reqordencomprasrv = models.CharField(db_column='reqOrdenCompraSRV', max_length=1)  # Field name made lowercase.
    pcadmoncredito = models.DecimalField(db_column='pcAdmonCredito', max_digits=4, decimal_places=2)  # Field name made lowercase.
    pcsobreprecio = models.DecimalField(db_column='pcSobrePrecio', max_digits=4, decimal_places=2)  # Field name made lowercase.
    solidarionombre = models.CharField(db_column='solidarioNombre', max_length=40)  # Field name made lowercase.
    solidariodireccion = models.CharField(db_column='solidarioDireccion', max_length=40)  # Field name made lowercase.
    solicariociudad = models.CharField(db_column='solicarioCiudad', max_length=40)  # Field name made lowercase.
    solidarioestado = models.CharField(db_column='solidarioEstado', max_length=40)  # Field name made lowercase.
    solidariomunicipio = models.CharField(db_column='solidarioMunicipio', max_length=40)  # Field name made lowercase.
    solidariopais = models.CharField(db_column='solidarioPais', max_length=40)  # Field name made lowercase.
    solidariotelefono = models.CharField(db_column='solidarioTelefono', max_length=40)  # Field name made lowercase.
    merca = models.CharField(max_length=2)
    updated_on = models.DateTimeField()

    SUCURSAL = 7
    EMPRESA = "abz"

    class Meta:
        managed = False
        db_table = 'cltCliente'

    @property
    def tiene_credito_congelado(self):
        ## Verifica si el cliente tiene credito congelado en gruas/servicio

        if self.EMPRESA=="abz":
            cursor = connection.cursor()
        if self.EMPRESA=="tasa":
            cursor = connections['tasa'].cursor()

        query = """
            SELECT congelado
            FROM cltClienteCredito
            WHERE id_cltCliente = %s
                AND id_ciaSucursal = %s
                AND id_ciaModulo = 7
        """ % (self.id, int(self.SUCURSAL))

        cursor.execute(query,[])
        congelado = False
        for row in cursor.fetchall():
            if row[0]== 'S':
                congelado = True

        return congelado
    
    @property
    def credito_asignado(self):
        ## Obtiene credito asignado en Gruas y en modulo de Servicio
        cursor = None
        if self.EMPRESA=="abz":
            cursor = connection.cursor()
        if self.EMPRESA=="tasa":
            cursor = connections['tasa'].cursor()

        query = """
            SELECT limite, diasCredito, congelado
            FROM cltClienteCredito
            WHERE id_cltCliente = %s
                AND id_ciaSucursal = %s
                AND id_ciaModulo = 7
        """ % (self.id, int(self.SUCURSAL))

        cursor.execute(query,[])
        credito = 0 
        for row in cursor.fetchall():
            if row[0]!= None:
                credito = row[0]

        return Decimal(credito)

    @property
    def credito_disponible(self):
        if self.credito_asignado > 0:
            return self.credito_asignado - self.saldo
        return Decimal(0)

    @property
    def saldo(self):
        # modulo 7  -> Servicio
        # sucursal 7  -> Gruas
        # id_cntCuenta  7 -> VCR ADM.Cliente Venta Crdito
        # id_cntPolizaST  3 -> Aplicada

        fecha_inicial = date(date.today().year, 1, 1)
        hoy = date.today()
        ayer = date.today() - timedelta(days=1)

        if self.EMPRESA=="abz":
            cursor = connection.cursor()
        if self.EMPRESA=="tasa":
            cursor = connections['tasa'].cursor()

        ## calcular saldo contable
        query_saldo_contable = """
            SELECT SUM(d.debe - d.haber) AS saldo
            FROM cntPolizaDetalle AS d
                INNER JOIN cntPoliza AS p ON ( p.id_cntPoliza = d.id_cntPoliza )
                INNER JOIN cltFactura AS f ON ( f.id_cltFactura = d.id_cltFactura )
            WHERE d.id_cntCuenta = 7
                AND d.id_ciaSucursal = %s
                AND d.id_cltCliente = %s
                AND f.id_ciaModulo = 7
                AND p.fecha_poliza >= '%s'
                AND p.id_cntPolizaST = 3
                
            """ % (int(self.SUCURSAL), self.id, fecha_inicial)

        cursor.execute(query_saldo_contable,[])
        saldo = 0 
        for row in cursor.fetchall():
            if row[0]!= None:
                saldo = row[0]


        # Movimientos del dia
        movs = 0;
        
        # Ingresos de ayer  (-)
        
        # id_cntPolizaTipo 10 -> ICO ADM:INGRESOS CONTADO
        # id_cntPolizaTipo 11 -> ICR ADM:INGRESOS CREDITO
        # $id_cltIngresoST 2 -> APLICADO
        
        query_ingresos_ayer = """
            SELECT i.idCajero, e.login, e.id_ciaPerfil, d.importe
            FROM cltIngresoDetalle AS d
                 INNER JOIN cltIngreso  AS i ON (i.id_cltIngreso = d.id_cltIngreso)
                 INNER JOIN empEmpleado AS e ON (e.id_empEmpleado = i.idCajero)
            WHERE i.fecha_ingreso = '%s'
                 AND i.id_cltIngresoST = 2
                 AND i.id_cltCliente = %s
                 AND d.id_cntCuenta = 7
                 AND i.id_ciaSucursal = %s
            """ % (ayer, self.id, int(self.SUCURSAL))
        
        cursor.execute(query_ingresos_ayer,[])

        for row in cursor.fetchall():
            mov_polizas = """
                SELECT id_cntPoliza FROM cntPoliza
                WHERE ( id_cntPolizaTipo = 10 OR id_cntPolizaTipo = 11 )
                    AND fecha_poliza = '%s'
                    AND id_ciaSucursal = %s
                    AND id_cntPolizaST = 3
                """ % (ayer, int(self.SUCURSAL))
            result_mov = cursor.execute(mov_polizas,[])
            # conteo = 0
            # if result_mov > 0:
            #     cursor.execute(mov_polizas,[])
            #     for m in cursor.fetchall():
            #         conteo += 1
            if result_mov==0:
                movs -= row[3]

        # Ingresos de hoy  (-)
        query_ingresos_hoy = """
            SELECT SUM(d.importe) AS sld
            FROM cltIngresoDetalle AS d
                INNER JOIN cltIngreso AS i ON (i.id_cltIngreso = d.id_cltIngreso)
            WHERE i.fecha_ingreso = '%s'
                AND i.id_cltIngresoST = 2
                AND i.id_cltCliente = %s
                AND d.id_cntCuenta = 7
                AND i.id_ciaSucursal = %s
            """ % (hoy, self.id, int(self.SUCURSAL))

        cursor.execute(query_ingresos_hoy,[])
        for row in cursor.fetchall():
            if row[0] != None:
                movs -= row[0]

        # # Facturas no contabilizadas (+)

        # # idTipoPoliza 30 -> VG GRS:VENTAS
        # # id_cltFacturaST 1 -> PREFACTURA
        # # id_cltFacturaST 2 -> FACTURADA

        query_facturas = """
            SELECT importe, fecha_factura, folio, id_ciaSucursal
            FROM cltFactura
            WHERE id_cntCuenta = 7
                AND id_cltCliente = %s
                AND fecha_factura >= '%s'
                AND id_ciaModulo = 7
                AND id_ciaSucursal = %s
                AND ( id_cltFacturaST = 1 OR id_cltFacturaST = 2 )
            """ % (self.id, ayer, int(self.SUCURSAL))
        facturas = cursor.execute(query_facturas,[])

        if facturas > 0:
            cursor.execute(query_facturas,[])
            for row in cursor.fetchall():
                sub_query_facturas = """
                    SELECT id_cntPoliza FROM cntPoliza
                    WHERE id_cntPolizaTipo = 30
                        AND fecha_poliza = '%s'
                        AND id_ciaSucursal = %s
                        AND id_cntPolizaST = 3
                """ % (row[1], int(self.SUCURSAL))

                result_fac = cursor.execute(sub_query_facturas,[])
                # conteo = 0
                # if result_fac > 0:
                #     cursor.execute(sub_query_facturas,[])
                #     for m in cursor.fetchall():
                #         conteo += 1
                if result_fac==0:
                    movs += row[0]
                

        saldo += movs

        return saldo


class Empleado(models.Model):
    id = models.AutoField(db_column='id_empEmpleado', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(max_length=30)
    paterno = models.CharField(db_column='apPaterno', max_length=20)  # Field name made lowercase.
    materno = models.CharField(db_column='apMaterno', max_length=20)  # Field name made lowercase.
    
    login = models.CharField(max_length=15)
    password = models.CharField(max_length=32)
    nombrepila = models.CharField(db_column='nombrePila', max_length=30)  # Field name made lowercase.
    rfc = models.CharField(max_length=13)
    curp = models.CharField(max_length=18)
    ciudad = models.CharField(max_length=30)
    colonia = models.CharField(max_length=30)
    calle = models.CharField(max_length=30)
    numero = models.CharField(max_length=10)
    cp = models.CharField(max_length=6)
    tel_casa = models.CharField(max_length=20)
    tel_celular = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    id_ciasegmento = models.IntegerField(db_column='id_ciaSegmento')  # Field name made lowercase.
    id_ciasucursal = models.IntegerField(db_column='id_ciaSucursal')  # Field name made lowercase.
    id_ciadepartamento = models.IntegerField(db_column='id_ciaDepartamento')  # Field name made lowercase.
    id_ciaperfil = models.IntegerField(db_column='id_ciaPerfil')  # Field name made lowercase.
    puesto = models.CharField(max_length=30)
    id_empempleadost = models.IntegerField(db_column='id_empEmpleadoST')  # Field name made lowercase.
    id_empgrupo = models.IntegerField(db_column='id_empGrupo')  # Field name made lowercase.
    fecha_password = models.DateField()
    wallpaper = models.CharField(max_length=30)
    theme = models.CharField(max_length=20)
    sso = models.CharField(max_length=32)
    emailpswd = models.CharField(max_length=50)
    emailtipo = models.CharField(max_length=4)
    emailsign = models.CharField(max_length=80)
    smtphost = models.CharField(max_length=50)
    smtpport = models.CharField(max_length=4)
    smtpauth = models.CharField(max_length=1)
    horas_minimas = models.DecimalField(max_digits=4, decimal_places=2)
    updated_on = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'empEmpleado'


class Sync(models.Model):

    model = models.ForeignKey(ContentType)
    last_synced = models.DateTimeField()

    class Meta:
        verbose_name = "Sync"
        verbose_name_plural = "Syncs"

    def __unicode__(self):
        return "%s" % (self.last_synced,)


class OrdenServicio(models.Model):
    id = models.AutoField(db_column='id_srvOS', primary_key=True)  # Field name made lowercase.
    folio = models.CharField(max_length=7)
    torreta = models.CharField(max_length=3)
    id_srvequipo = models.IntegerField(db_column='id_srvEquipo')  # Field name made lowercase.
    id_cltcliente = models.IntegerField(db_column='id_cltCliente')  # Field name made lowercase.
    id_cltpreferencialtarjeta = models.IntegerField(db_column='id_cltPreferencialTarjeta')  # Field name made lowercase.
    idasesor = models.IntegerField(db_column='idAsesor')  # Field name made lowercase.
    idventanilla = models.IntegerField(db_column='idVentanilla')  # Field name made lowercase.
    id_srvostipo = models.IntegerField(db_column='id_srvOSTipo')  # Field name made lowercase.
    id_comov = models.IntegerField(db_column='id_comOV')  # Field name made lowercase.
    id_grsos = models.IntegerField(db_column='id_grsOS')  # Field name made lowercase.
    id_srvcontrato = models.IntegerField(db_column='id_srvContrato')  # Field name made lowercase.
    id_pjtproyecto = models.IntegerField(db_column='id_pjtProyecto')  # Field name made lowercase.
    inspeccion = models.CharField(max_length=1)
    ordencompra = models.CharField(db_column='ordenCompra', max_length=15)  # Field name made lowercase.
    siniestro = models.CharField(max_length=10)
    kilometraje = models.IntegerField()
    id_srvtipoinmovilizacion = models.IntegerField(db_column='id_srvTipoInmovilizacion')  # Field name made lowercase.
    des_inmovilizacion = models.CharField(max_length=200)
    # fecha_inmovilizacion = models.DateField(null=True, blank=True)
    hora_inmovilizacion = models.TimeField()
    id_srvosst = models.IntegerField(db_column='id_srvOSST')  # Field name made lowercase.
    id_ciasucursal = models.IntegerField(db_column='id_ciaSucursal')  # Field name made lowercase.
    facturaseparada = models.CharField(db_column='facturaSeparada', max_length=1)  # Field name made lowercase.
    cierraovcomercial = models.CharField(db_column='cierraOVComercial', max_length=1)  # Field name made lowercase.
    desgloseiva = models.CharField(db_column='desgloseIVA', max_length=1)  # Field name made lowercase.
    ventacredito = models.CharField(db_column='ventaCredito', max_length=1)  # Field name made lowercase.
    tasadescmo = models.DecimalField(db_column='tasaDescMO', max_digits=4, decimal_places=2)  # Field name made lowercase.
    tasadescrfc = models.DecimalField(db_column='tasaDescRFC', max_digits=4, decimal_places=2)  # Field name made lowercase.
    id_ciatipoiva = models.IntegerField(db_column='id_ciaTipoIVA')  # Field name made lowercase.
    id_ciamoneda = models.IntegerField(db_column='id_ciaMoneda')  # Field name made lowercase.
    tipocambio = models.DecimalField(db_column='tipoCambio', max_digits=8, decimal_places=4)  # Field name made lowercase.
    srv_subtotal = models.DecimalField(max_digits=10, decimal_places=2)
    srv_iva = models.DecimalField(max_digits=8, decimal_places=2)
    srv_retiva = models.DecimalField(db_column='srv_retIVA', max_digits=8, decimal_places=2)  # Field name made lowercase.
    srv_retisr = models.DecimalField(db_column='srv_retISR', max_digits=8, decimal_places=2)  # Field name made lowercase.
    srv_importe = models.DecimalField(max_digits=10, decimal_places=2)
    com_subtotal = models.DecimalField(max_digits=10, decimal_places=2)
    com_iva = models.DecimalField(max_digits=8, decimal_places=2)
    com_retiva = models.DecimalField(db_column='com_retIVA', max_digits=8, decimal_places=2)  # Field name made lowercase.
    com_retisr = models.DecimalField(db_column='com_retISR', max_digits=8, decimal_places=2)  # Field name made lowercase.
    com_importe = models.DecimalField(max_digits=10, decimal_places=2)
    puntos = models.DecimalField(max_digits=8, decimal_places=2)
    monedero = models.DecimalField(max_digits=8, decimal_places=2)
    fecha_apertura = models.DateField()
    hora_apertura = models.TimeField()
    # fecha_cierre = models.DateField()
    hora_cierre = models.TimeField()
    # fecha_factura = models.DateField()
    hora_factura = models.TimeField()
    observacion = models.TextField()
    id_cltfactura = models.IntegerField(db_column='id_cltFactura')  # Field name made lowercase.
    contacto = models.CharField(max_length=30)
    contactoemail = models.CharField(db_column='contactoEmail', max_length=40)  # Field name made lowercase.
    contactotel = models.CharField(db_column='contactoTel', max_length=30)  # Field name made lowercase.
    contactocel = models.CharField(db_column='contactoCel', max_length=30)  # Field name made lowercase.
    idempcierre = models.IntegerField(db_column='idEmpCierre')  # Field name made lowercase.
    idempfactura = models.IntegerField(db_column='idEmpFactura')  # Field name made lowercase.
    # fecha_cancelacion = models.DateField(null=True, blank=True)
    hora_cancelacion = models.TimeField(null=True, blank=True)
    idempcancela = models.IntegerField(db_column='idEmpCancela')  # Field name made lowercase.
    llantamarca = models.CharField(db_column='llantaMarca', max_length=20)  # Field name made lowercase.
    llantamedida = models.CharField(db_column='llantaMedida', max_length=20)  # Field name made lowercase.
    fecha_promentrega = models.DateField()
    hora_promentrega = models.TimeField()
    tasaiva = models.DecimalField(db_column='tasaIVA', max_digits=4, decimal_places=2)  # Field name made lowercase.
    servparticular = models.IntegerField(db_column='servParticular')  # Field name made lowercase.
    id_srvossubtipo = models.IntegerField(db_column='id_srvOSSubTipo')  # Field name made lowercase.
    idsucinterno = models.IntegerField(db_column='idSucInterno')  # Field name made lowercase.
    idareainterno = models.IntegerField(db_column='idAreaInterno')  # Field name made lowercase.
    idosinterno = models.IntegerField(db_column='idOSInterno')  # Field name made lowercase.
    idgtointerno = models.IntegerField(db_column='idGtoInterno')  # Field name made lowercase.
    origenentrada = models.CharField(db_column='origenEntrada', max_length=20)  # Field name made lowercase.
    express = models.CharField(db_column='express', max_length=1)  # Field name made lowercase.
    refacturacion = models.CharField(db_column='refacturacion', max_length=1)  # Field name made lowercase.
    # fecha_refacturacion = models.DateField()

    #### CAMBIOS ####
    # Se modificaron varios campos en la base de datos, para lograr que se guardaran a traves del serializador
    # Se les quito NotNull a:
        # fecha_inmovilizacion
        # fecha_apertura
        # fecha_cierre
        # fecha_promentrega
        # fecha_factura
        # fecha_cancelacion

        #MAYO 2017 implementacion. TASA
        # origenentrada
        # express
        # refacturacion
        # fecha_refacturacion

    class Meta:
        managed = False
        db_table = 'srvOS'


    def __unicode__(self):
        return "%s" % (self.folio,)

    # @property
    # def empresa(self):
    #     return ""
    
    def crear_ov(self):
        now = datetime.datetime.now()

        orden_venta = OrdenVenta()
        orden_venta.id_srvos = self.pk
        orden_venta.id_empempleado = self.idasesor
        orden_venta.id_cltcliente = self.id_cltcliente
        orden_venta.id_srvostipo = self.id_srvostipo

        orden_venta.id_ciasucursal = self.id_ciasucursal
        orden_venta.id_cltpreferencialtarjeta = 0
        orden_venta.id_pjtproyecto = 0
        orden_venta.fecha_ov = now.strftime('%Y-%m-%d')
        orden_venta.hora_ov = now
        orden_venta.id_comovtipo = 4 #Servicio
        orden_venta.id_comovst = 1 ##Cotizacion
        orden_venta.desgloseiva = "S"
        orden_venta.ventacredito = "S"
        orden_venta.tasadescuento = 0.00
        orden_venta.iva = 0.00
        orden_venta.retiva = 0.00
        orden_venta.retisr = 0.00
        orden_venta.importe = 0.00
        orden_venta.id_cltfactura = 0
        orden_venta.id_ciamoneda = 1
        orden_venta.tipocambio = 1.0000
        orden_venta.descuento = 0.00
        orden_venta.ordencompra = ""
        orden_venta.referencia = ""
        orden_venta.nota = ""
        orden_venta.entregapersona = ""
        orden_venta.entregatelefono = ""
        orden_venta.entregadireccion = ""
        orden_venta.anterior = ""
        orden_venta.puntos = 0.00
        orden_venta.monedero = 0.00
        orden_venta.id_ciaaduana = 0
        orden_venta.hora_cierre = "00:00:00"
        orden_venta.idempcierre = 0
        orden_venta.hora_factura = "00:00:00"
        orden_venta.idempfactura = 0
        orden_venta.hora_cancelacion = "00:00:00"
        orden_venta.idempcancela = 0
        orden_venta.id_cltingresoforma = 0
        orden_venta.cuentapago = ""
        orden_venta.codigoale = ""
        orden_venta.save()
        self.id_comov = orden_venta.pk
        self.save()

    def save(self, *args, **kwargs):
        # es_nueva = False
        # if not self.pk:
        #     es_nueva = True
        super(OrdenServicio, self).save(*args, **kwargs)
        # if es_nueva==True:
        #     self.crear_ov()

class OrdenServicioDetalle(models.Model):
    id = models.AutoField(db_column='id_srvOSDetalle', primary_key=True)  # Field name made lowercase.
    orden_servicio = models.ForeignKey('api.OrdenServicio',db_column='id_srvOS', related_name='detalles')  # Field name made lowercase.
    
    cantidad = models.DecimalField(max_digits=8, decimal_places=2)
    tipodetalle = models.CharField(db_column='tipoDetalle', max_length=1)  # Field name made lowercase.
    id_srvoperacion = models.IntegerField(db_column='id_srvOperacion')  # Field name made lowercase.
    taskcode = models.CharField(db_column='taskCode', max_length=12)  # Field name made lowercase.
    taskname = models.CharField(db_column='taskName', max_length=75)  # Field name made lowercase.
    id_empempleado = models.IntegerField(db_column='id_empEmpleado')  # Field name made lowercase.
    id_srvoperacionst = models.IntegerField(db_column='id_srvOperacionST')  # Field name made lowercase.
    id_pvrproveedor = models.IntegerField(db_column='id_pvrProveedor')  # Field name made lowercase.
    horasprecio = models.DecimalField(db_column='horasPrecio', max_digits=4, decimal_places=2)  # Field name made lowercase.
    horascosto = models.DecimalField(db_column='horasCosto', max_digits=4, decimal_places=2)  # Field name made lowercase.
    foliofactura = models.CharField(db_column='folioFactura', max_length=10)  # Field name made lowercase.
    # fecha_factura = models.DateField()
    # fecha_compromiso = models.DateField()
    costo = models.DecimalField(max_digits=10, decimal_places=2)
    preciounitario = models.DecimalField(db_column='precioUnitario', max_digits=10, decimal_places=2)  # Field name made lowercase.
    precio = models.DecimalField(max_digits=10, decimal_places=2)
    ivafactura = models.DecimalField(db_column='ivaFactura', max_digits=8, decimal_places=2)  # Field name made lowercase.
    id_ciatipoiva = models.IntegerField(db_column='id_ciaTipoIVA')  # Field name made lowercase.
    tasaiva = models.DecimalField(db_column='tasaIVA', max_digits=4, decimal_places=2)  # Field name made lowercase.
    fecha_ini = models.DateField()
    hora_ini = models.TimeField()
    fecha_fin = models.DateField()
    hora_fin = models.TimeField()
    hrinicio = models.TimeField(db_column='hrInicio')  # Field name made lowercase.
    hrpausa = models.TimeField(db_column='hrPausa')  # Field name made lowercase.
    hrsacumuladas = models.DecimalField(db_column='hrsAcumuladas', max_digits=4, decimal_places=2)  # Field name made lowercase.
    id_pvrfactura = models.IntegerField(db_column='id_pvrFactura')  # Field name made lowercase.
    puntos = models.DecimalField(max_digits=8, decimal_places=2)
    monedero = models.DecimalField(max_digits=8, decimal_places=2)
    id_srvpaquete = models.IntegerField(db_column='id_srvPaquete')  # Field name made lowercase.
    id_srvpromocion = models.IntegerField(db_column='id_srvPromocion')  # Field name made lowercase.
    nota = models.CharField(max_length=200)
    razon_detiene = models.CharField(max_length=250)

    ## CAMPOS NULOS 
    # fecha_factura
    # fecha_compromiso

    class Meta:
        managed = False
        db_table = 'srvOSDetalle'


    def __unicode__(self):
        return "%s" % (self.id,)
    
    def save(self, *args, **kwargs):
        # print self.orden_servicio.pk
        # if self.orden_servicio:
        #     self.id_ciatipoiva = self.orden_servicio.id_ciatipoiva
        #     self.tasaiva = self.orden_servicio.tasaiva

        super(OrdenServicioDetalle, self).save(*args, **kwargs)


class OrdenVenta(models.Model):
    id = models.AutoField(db_column='id_comOV', primary_key=True)  # Field name made lowercase.
    id_empempleado = models.IntegerField(db_column='id_empEmpleado')  # Field name made lowercase.
    id_ciasucursal = models.IntegerField(db_column='id_ciaSucursal')  # Field name made lowercase.
    id_cltcliente = models.IntegerField(db_column='id_cltCliente')  # Field name made lowercase.
    id_cltpreferencialtarjeta = models.IntegerField(db_column='id_cltPreferencialTarjeta')  # Field name made lowercase.
    id_pjtproyecto = models.IntegerField(db_column='id_pjtProyecto')  # Field name made lowercase.
    folio = models.IntegerField()
    fecha_ov = models.DateField()
    hora_ov = models.TimeField()
    id_comovtipo = models.IntegerField(db_column='id_comOVTipo')  # Field name made lowercase.
    id_comovst = models.IntegerField(db_column='id_comOVST')  # Field name made lowercase.
    id_srvos = models.IntegerField(db_column='id_srvOS')  # Field name made lowercase.
    desgloseiva = models.CharField(db_column='desgloseIVA', max_length=1)  # Field name made lowercase.
    ventacredito = models.CharField(db_column='ventaCredito', max_length=1)  # Field name made lowercase.
    tasadescuento = models.DecimalField(db_column='tasaDescuento', max_digits=4, decimal_places=2)  # Field name made lowercase.
    iva = models.DecimalField(max_digits=8, decimal_places=2)
    retiva = models.DecimalField(db_column='retIVA', max_digits=8, decimal_places=2)  # Field name made lowercase.
    retisr = models.DecimalField(db_column='retISR', max_digits=8, decimal_places=2)  # Field name made lowercase.
    importe = models.DecimalField(max_digits=10, decimal_places=2)
    id_cltfactura = models.IntegerField(db_column='id_cltFactura')  # Field name made lowercase.
    id_ciamoneda = models.IntegerField(db_column='id_ciaMoneda')  # Field name made lowercase.
    tipocambio = models.DecimalField(db_column='tipoCambio', max_digits=8, decimal_places=4)  # Field name made lowercase.
    descuento = models.DecimalField(max_digits=10, decimal_places=2)
    ordencompra = models.CharField(db_column='ordenCompra', max_length=20)  # Field name made lowercase.
    referencia = models.CharField(max_length=20)
    nota = models.TextField()
    entregapersona = models.CharField(db_column='entregaPersona', max_length=40)  # Field name made lowercase.
    entregatelefono = models.CharField(db_column='entregaTelefono', max_length=20)  # Field name made lowercase.
    entregadireccion = models.TextField(db_column='entregaDireccion')  # Field name made lowercase.
    anterior = models.CharField(db_column='ANTERIOR', max_length=2)  # Field name made lowercase.
    puntos = models.DecimalField(max_digits=8, decimal_places=2)
    monedero = models.DecimalField(max_digits=8, decimal_places=2)
    id_ciaaduana = models.IntegerField(db_column='id_ciaAduana')  # Field name made lowercase.
    # fecha_cierre = models.DateField()
    hora_cierre = models.TimeField()
    idempcierre = models.IntegerField(db_column='idEmpCierre')  # Field name made lowercase.
    # fecha_factura = models.DateField()
    hora_factura = models.TimeField()
    idempfactura = models.IntegerField(db_column='idEmpFactura')  # Field name made lowercase.
    # fecha_cancelacion = models.DateField()
    hora_cancelacion = models.TimeField()
    idempcancela = models.IntegerField(db_column='idEmpCancela')  # Field name made lowercase.
    id_cltingresoforma = models.IntegerField(db_column='id_cltIngresoForma')  # Field name made lowercase.
    cuentapago = models.CharField(db_column='cuentaPago', max_length=10)  # Field name made lowercase.
    id_srvostipo = models.IntegerField(db_column='id_srvOSTipo')  # Field name made lowercase.
    codigoale = models.CharField(db_column='codigoAle', max_length=9)  # Field name made lowercase.

    #### CAMBIOS ####
    # Se modificaron varios campos en la base de datos, para lograr que se guardaran a traves del serializador
    # Se les quito NotNull a:
        # fecha_cierre
        # fecha_factura
        # fecha_cancelacion

    class Meta:
        managed = False
        db_table = 'comOV'



    def save(self, *args, **kwargs):
        # if not self.pk:
        #     qs = OrdenVenta.objects.filter(id_ciasucursal=self.id_ciasucursal).aggregate(Max('folio'))
        #     if qs["folio__max"] > 0:
        #         self.folio = (qs["folio__max"] + 1)
        #     else:
        #         self.folio = 1

        super(OrdenVenta, self).save(*args, **kwargs)



# class Poliza(models.Model):
#     id = models.AutoField(db_column='id_cntPoliza', primary_key=True)  # Field name made lowercase.
#     tipo = models.IntegerField(db_column='id_cntPolizaTipo')  # Field name made lowercase.
#     folio = models.IntegerField()
#     id_ciasucursal = models.IntegerField(db_column='id_ciaSucursal')  # Field name made lowercase.
#     fecha_poliza = models.DateField()
#     nombre = models.CharField(max_length=50)
#     nombreegreso = models.CharField(db_column='nombreEgreso', max_length=50)  # Field name made lowercase.
#     importe = models.DecimalField(max_digits=12, decimal_places=2)
#     idgenero = models.IntegerField(db_column='idGenero')  # Field name made lowercase.
#     ideditor = models.IntegerField(db_column='idEditor')  # Field name made lowercase.
#     idaprobo = models.IntegerField(db_column='idAprobo')  # Field name made lowercase.
#     fecha_edicion = models.DateField()
#     hora_edicion = models.TimeField()
#     id_cntpolizast = models.IntegerField(db_column='id_cntPolizaST')  # Field name made lowercase.
#     id_bcsbanco = models.IntegerField(db_column='id_bcsBanco')  # Field name made lowercase.
#     id_cntconceptoflujo = models.IntegerField(db_column='id_cntConceptoFlujo')  # Field name made lowercase.
#     numcheque = models.IntegerField(db_column='numCheque')  # Field name made lowercase.
#     beneficiario = models.CharField(max_length=55)
#     referencia = models.CharField(max_length=10)
#     leyenda = models.CharField(max_length=1)
#     conciliado = models.CharField(max_length=1)
#     importeegreso = models.DecimalField(db_column='importeEgreso', max_digits=10, decimal_places=2)  # Field name made lowercase.
#     tipocambio = models.DecimalField(db_column='tipoCambio', max_digits=8, decimal_places=4)  # Field name made lowercase.
#     numimpresiones = models.IntegerField(db_column='numImpresiones')  # Field name made lowercase.

#     class Meta:
#         managed = False
#         db_table = 'cntPoliza'        

# class PolizaDetalle(models.Model):
#     id = models.AutoField(db_column='id_cntPolizaDetalle', primary_key=True)  # Field name made lowercase.
#     poliza = models.ForeignKey('api.Poliza',db_column='id_cntPoliza', related_name='detalles')  # Field name made lowercase.
#     cliente = models.ForeignKey('api.Cliente',db_column='id_cltCliente')  # Field name made lowercase.
#     empleado = models.ForeignKey('api.Empleado',db_column='id_empEmpleado', related_name='polizas')  # Field name made lowercase.
#     factura = models.ForeignKey('api.Factura',db_column='id_cltFactura', related_name='polizas')  # Field name made lowercase.
#     ingreso_detalle = models.ForeignKey('api.IngresoDetalle',db_column='id_cltIngresoDetalle', related_name='polizas')  # Field name made lowercase.
#     id_ciasucursal = models.IntegerField(db_column='id_ciaSucursal')  # Field name made lowercase.
    
#     id_cntcuenta = models.IntegerField(db_column='id_cntCuenta')  # Field name made lowercase.
#     id_pvrproveedor = models.IntegerField(db_column='id_pvrProveedor')  # Field name made lowercase.
#     id_ciaarea = models.IntegerField(db_column='id_ciaArea')  # Field name made lowercase.
#     id_pvrfactura = models.IntegerField(db_column='id_pvrFactura')  # Field name made lowercase.
#     id_pjtproyecto = models.IntegerField(db_column='id_pjtProyecto')  # Field name made lowercase.
#     id_cntconceptoflujo = models.IntegerField(db_column='id_cntConceptoFlujo')  # Field name made lowercase.
#     debe = models.DecimalField(max_digits=10, decimal_places=2)
#     haber = models.DecimalField(max_digits=10, decimal_places=2)
#     tipocambio = models.DecimalField(db_column='tipoCambio', max_digits=8, decimal_places=4)  # Field name made lowercase.
#     concepto = models.CharField(max_length=60)
#     referencia = models.CharField(max_length=10)
#     id_bcsestadodetalle = models.IntegerField(db_column='id_bcsEstadoDetalle')  # Field name made lowercase.

#     class Meta:
#         managed = False
#         db_table = 'cntPolizaDetalle'        



# class Ingreso(models.Model):
#     id = models.AutoField(db_column='id_cltIngreso', primary_key=True)  # Field name made lowercase.
#     cliente = models.ForeignKey('api.Cliente',db_column='id_cltCliente')  # Field name made lowercase.
#     id_ciasucursal = models.IntegerField(db_column='id_ciaSucursal')  # Field name made lowercase.

#     id_bcsbanco = models.IntegerField(db_column='id_bcsBanco')  # Field name made lowercase.
#     prefijo = models.CharField(max_length=3)
#     folio = models.IntegerField()
#     referencia = models.CharField(max_length=30)
#     descripcion = models.TextField()
#     importe = models.DecimalField(max_digits=10, decimal_places=2)
#     saldo = models.DecimalField(max_digits=10, decimal_places=2)
#     tasaiva = models.DecimalField(db_column='tasaIVA', max_digits=6, decimal_places=2)  # Field name made lowercase.
#     desgloseiva = models.CharField(db_column='desgloseIVA', max_length=1)  # Field name made lowercase.
#     devolucion = models.CharField(max_length=1)
#     fecha_ingreso = models.DateField()
#     hora_ingreso = models.TimeField()
#     id_cltingresoforma = models.IntegerField(db_column='id_cltIngresoForma')  # Field name made lowercase.
#     id_cltingresost = models.IntegerField(db_column='id_cltIngresoST')  # Field name made lowercase.
#     numimpresiones = models.IntegerField(db_column='numImpresiones')  # Field name made lowercase.
#     idcajero = models.IntegerField(db_column='idCajero')  # Field name made lowercase.
#     idcobrador = models.IntegerField(db_column='idCobrador')  # Field name made lowercase.
#     nombrefiscal = models.CharField(db_column='nombreFiscal', max_length=90)  # Field name made lowercase.
#     rfc = models.CharField(max_length=13)
#     id_locestado = models.IntegerField(db_column='id_locEstado')  # Field name made lowercase.
#     municipio = models.CharField(max_length=30)
#     ciudad = models.CharField(max_length=30)
#     colonia = models.CharField(max_length=30)
#     calle = models.CharField(max_length=45)
#     numero = models.CharField(max_length=6)
#     numerointerior = models.CharField(db_column='numeroInterior', max_length=5)  # Field name made lowercase.
#     cp = models.CharField(max_length=6)

#     class Meta:
#         managed = False
#         db_table = 'cltIngreso'

# class IngresoDetalle(models.Model):
#     id = models.AutoField(db_column='id_cltIngresoDetalle', primary_key=True)  # Field name made lowercase.
#     ingreso = models.ForeignKey('api.Ingreso',db_column='id_cltIngreso', related_name='detalles')  # Field name made lowercase.
#     factura = models.ForeignKey('api.Factura',db_column='id_cltFactura', related_name='ingresos')  # Field name made lowercase.
#     poliza = models.ForeignKey('api.Poliza',db_column='id_cntPoliza', related_name='ingresos')  # Field name made lowercase.
    
#     fecha_aplicacion = models.DateField()
#     id_cntcuenta = models.IntegerField(db_column='id_cntCuenta')  # Field name made lowercase.
#     id_cltingresoforma = models.IntegerField(db_column='id_cltIngresoForma')  # Field name made lowercase.
#     tipoaplicacion = models.CharField(db_column='tipoAplicacion', max_length=2)  # Field name made lowercase.
#     id_pvrfactura = models.IntegerField(db_column='id_pvrFactura')  # Field name made lowercase.
#     idingresodetalle = models.IntegerField(db_column='idIngresoDetalle')  # Field name made lowercase.
#     polingreso = models.CharField(db_column='polIngreso', max_length=1)  # Field name made lowercase.
#     descripcion = models.CharField(max_length=25)
#     referencia = models.CharField(max_length=10)
#     bonificacion = models.CharField(max_length=1)
#     upcambiaria = models.DecimalField(db_column='upCambiaria', max_digits=6, decimal_places=2)  # Field name made lowercase.
#     tasadescprontopago = models.DecimalField(db_column='tasaDescProntoPago', max_digits=6, decimal_places=2)  # Field name made lowercase.
#     tcpuntos = models.DecimalField(db_column='tcPuntos', max_digits=10, decimal_places=2)  # Field name made lowercase.
#     importe = models.DecimalField(max_digits=10, decimal_places=2)
#     iva = models.DecimalField(max_digits=8, decimal_places=2)
#     saldo = models.DecimalField(max_digits=10, decimal_places=2)
#     id_ciatipoiva = models.IntegerField(db_column='id_ciaTipoIVA')  # Field name made lowercase.
#     id_cltingresoformamarca = models.IntegerField(db_column='id_cltIngresoFormaMarca')  # Field name made lowercase.
#     id_cltingresoformadetalle = models.IntegerField(db_column='id_cltIngresoFormaDetalle')  # Field name made lowercase.
#     voucher = models.CharField(max_length=10)
#     id_cltnota = models.IntegerField(db_column='id_cltNota')  # Field name made lowercase.

#     class Meta:
#         managed = False
#         db_table = 'cltIngresoDetalle'        



# class Factura(models.Model):
#     id = models.AutoField(db_column='id_cltFactura', primary_key=True)  # Field name made lowercase.
#     id_cltfacturatipo = models.IntegerField(db_column='id_cltFacturaTipo')  # Field name made lowercase.
#     id_ciamodulo = models.IntegerField(db_column='id_ciaModulo')  # Field name made lowercase.
#     id_ciasucursal = models.IntegerField(db_column='id_ciaSucursal')  # Field name made lowercase.
#     id_cntcuenta = models.IntegerField(db_column='id_cntCuenta')  # Field name made lowercase.
#     prefijo = models.CharField(max_length=3)
#     folio = models.IntegerField()
#     posfijo = models.CharField(max_length=3)
#     fecha_factura = models.DateField()
#     hora_factura = models.TimeField()
#     fecha_compromiso = models.DateField()
#     fecha_contrarecibo = models.DateField()
#     fecha_devolucion = models.DateField()
#     id_cltcliente = models.IntegerField(db_column='id_cltCliente')  # Field name made lowercase.
#     id_empempleado = models.IntegerField(db_column='id_empEmpleado')  # Field name made lowercase.
#     multiorden = models.CharField(db_column='multiOrden', max_length=1)  # Field name made lowercase.
#     iddocfuente = models.IntegerField(db_column='idDocFuente')  # Field name made lowercase.
#     foliodocfuente = models.CharField(db_column='folioDocFuente', max_length=10)  # Field name made lowercase.
#     id_pjtproyecto = models.IntegerField(db_column='id_pjtProyecto')  # Field name made lowercase.
#     credito = models.CharField(max_length=1)
#     id_ciamoneda = models.IntegerField(db_column='id_ciaMoneda')  # Field name made lowercase.
#     tipocambio = models.DecimalField(db_column='tipoCambio', max_digits=6, decimal_places=4)  # Field name made lowercase.
#     upcambiaria = models.DecimalField(db_column='upCambiaria', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     id_ciatipoiva = models.IntegerField(db_column='id_ciaTipoIVA')  # Field name made lowercase.
#     tasaiva = models.DecimalField(db_column='tasaIVA', max_digits=6, decimal_places=2)  # Field name made lowercase.
#     subtotal = models.DecimalField(max_digits=10, decimal_places=2)
#     iva = models.DecimalField(max_digits=8, decimal_places=2)
#     retiva = models.DecimalField(db_column='retIVA', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     retisr = models.DecimalField(db_column='retISR', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     importe = models.DecimalField(max_digits=10, decimal_places=2)
#     saldo = models.DecimalField(max_digits=10, decimal_places=2)
#     descdifprecios = models.DecimalField(db_column='descDifPrecios', max_digits=10, decimal_places=2)  # Field name made lowercase.
#     ivadescdifprecios = models.DecimalField(db_column='ivaDescDifPrecios', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     impprontopago = models.DecimalField(db_column='impProntoPago', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     id_bcsbanco = models.IntegerField(db_column='id_bcsBanco')  # Field name made lowercase.
#     chdeviva = models.DecimalField(db_column='chDevIVA', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     financiar = models.DecimalField(max_digits=10, decimal_places=2)
#     numpagares = models.IntegerField(db_column='numPagares')  # Field name made lowercase.
#     tasapagares = models.DecimalField(db_column='tasaPagares', max_digits=4, decimal_places=2)  # Field name made lowercase.
#     intpagares = models.DecimalField(db_column='intPagares', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     ivapagares = models.DecimalField(db_column='ivaPagares', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     valpagare = models.DecimalField(db_column='valPagare', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     id_cltfacturast = models.IntegerField(db_column='id_cltFacturaST')  # Field name made lowercase.
#     numimpresiones = models.IntegerField(db_column='numImpresiones')  # Field name made lowercase.
#     fctremisiones = models.CharField(db_column='fctRemisiones', max_length=1)  # Field name made lowercase.
#     idfactadm = models.IntegerField(db_column='idFactAdm')  # Field name made lowercase.
#     id_cltpreferencialtarjeta = models.IntegerField(db_column='id_cltPreferencialTarjeta')  # Field name made lowercase.
#     nombrefiscal = models.CharField(db_column='nombreFiscal', max_length=90)  # Field name made lowercase.
#     rfc = models.CharField(max_length=13)
#     id_locestado = models.IntegerField(db_column='id_locEstado')  # Field name made lowercase.
#     municipio = models.CharField(max_length=30)
#     ciudad = models.CharField(max_length=30)
#     colonia = models.CharField(max_length=30)
#     calle = models.CharField(max_length=45)
#     numero = models.CharField(max_length=6)
#     numerointerior = models.CharField(db_column='numeroInterior', max_length=10)  # Field name made lowercase.
#     cp = models.CharField(max_length=6)
#     nota = models.TextField()
#     fecha_cancelacion = models.DateField()
#     pjt_sldfacturas = models.CharField(max_length=1)
#     pjt_idclt = models.IntegerField()
#     pjt_fecha_ini = models.DateField()
#     pjt_fecha_fin = models.DateField()
#     cfd_numaprobacion = models.IntegerField(db_column='cfd_numAprobacion')  # Field name made lowercase.
#     cfd_anoaprobacion = models.IntegerField(db_column='cfd_anoAprobacion')  # Field name made lowercase.
#     cfd_seriecertsd = models.CharField(db_column='cfd_serieCertSD', max_length=20)  # Field name made lowercase.
#     ordencompra = models.CharField(db_column='ordenCompra', max_length=15)  # Field name made lowercase.
#     tasaieps = models.DecimalField(db_column='tasaIEPS', max_digits=6, decimal_places=2)  # Field name made lowercase.
#     ieps = models.DecimalField(max_digits=8, decimal_places=2)
#     cfd_fchaprobacion = models.CharField(db_column='cfd_fchAprobacion', max_length=20)  # Field name made lowercase.
#     retimplocal = models.DecimalField(db_column='retImpLocal', max_digits=8, decimal_places=2)  # Field name made lowercase.
#     id_cltingresoforma = models.IntegerField(db_column='id_cltIngresoForma')  # Field name made lowercase.
#     cuentapago = models.CharField(db_column='cuentaPago', max_length=10)  # Field name made lowercase.
#     servparticular = models.IntegerField(db_column='servParticular')  # Field name made lowercase.
#     cfd_numcertificado = models.CharField(db_column='cfd_numCertificado', max_length=20)  # Field name made lowercase.
#     cfd_uuid = models.CharField(max_length=40)
#     cfd_uuid_cancelacion = models.CharField(max_length=40)

#     class Meta:
#         managed = False
#         db_table = 'cltFactura'

