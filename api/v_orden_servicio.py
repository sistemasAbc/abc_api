from models import OrdenServicio, Sync
from serializers import OrdenServicioSerializer
from django.contrib.contenttypes.models import ContentType
from datetime import datetime

## rest framework
from rest_framework import viewsets, filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework import status

class OrdenServicioViewSet(viewsets.ModelViewSet):
    """
    API endpoint para insertar ordenes de servicio
    """
    def get_queryset(self):
        queryset = OrdenServicio.objects.all()

        if self.request.GET:
            folio = self.request.GET.get('folio',"")
            estatus = self.request.GET.getlist('estatus',None)

            if folio:
                queryset = queryset.filter(folio__icontains=folio)
            if estatus:
                queryset = queryset.filter(estatus__in=estatus)
            
        return queryset

    queryset = OrdenServicio.objects.all()
    serializer_class = OrdenServicioSerializer
    http_method_names = ['get', 'post',]