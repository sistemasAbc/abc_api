from django.contrib import admin

# Register your models here.

from models import *

admin.site.register(Cliente)
admin.site.register(Empleado)
admin.site.register(OrdenServicio)
admin.site.register(OrdenServicioDetalle)
admin.site.register(Sync)